const Discord = require('discord.js')

const request = require('request');

const bot = new Discord.Client();

var generalId = '850459287847567382';
var adminIds = ['850317067956518943', '850469243232714753', '860831115308302356'];
var deadRoleId = '858641011270352897';

bot.login("ODY3MzgxNjI2MjA0MDYxNzE3.6xz2r23kRj4wcc832b4QnXGE_Hw"); //this is the (edited) secret login code that the discord api gives you

function rollDiceWithMaxNumber(maxNumber) { // generates a random int between 1 and maxNumber
    return Math.floor(Math.random() * maxNumber) + 1;
}

//when the bot is loaded
bot.on('ready', () => {
    console.log('Bot is booted !');

    setInterval( //this is a function that every hour has a 1 in a 1000 chance to send weird private messages (for fun) to the last member that posted a message in the general channel
        async function(){
            if (rollDiceWithMaxNumber(1000) == 500){
                let rng = rollDiceWithMaxNumber(100);
                const embed = new Discord.MessageEmbed();
                embed.setColor(0x4a4a4a);

                //gets a random image between these 3
                if (rng <= 33){
                    embed.setTitle("I know where you sleep at night");
                    embed.setImage("https://i.ytimg.com/vi/KXv9x6y1zfw/hqdefault.jpg");
                }
                else if (rng <= 66){
                    embed.setTitle("Hey cutie");
                    embed.setImage("https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.wNDkyT4B08hdWtgarGopCQHaFj%26pid%3DApi&f=1");
                }
                else {
                    embed.setTitle("I drew this for you");
                    embed.setImage("https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.pinimg.com%2Foriginals%2Fc0%2Fd3%2Fea%2Fc0d3ea0eac7a98235696c73f771bb250.jpg&f=1&nofb=1");
                }

                //gets the author of the last message in the general channel and sends the embed to them
                bot.channels.cache.get(generalId).messages.fetch({ limit: 1 }).then(messages =>{
                    let lastMessage = messages.first();
                    lastMessage.author.send(embed);
                });
            }
    }, 3600000)
})

//when a message is posted on the bot's server
bot.on('message', msg =>{
    if (msg.author.bot === false) {
        let words = msg.content.toLowerCase().split(' ');
        if (!msg.mentions.users.size == 0) { //if a user is mentioned in the message
            let mentionedUser = msg.mentions.users.get(msg.mentions.users.firstKey());
            if (mentionedUser.bot === false){
                if (words[0].match("!shoot")) { //checking for name of the command (usage is !shoot @user)

                    for (id : adminIds){
                       if(msg.guild.member(msg.author).roles.cache.has(id)){
                            shootCommand(msg);
                            return;
                       }
                    }

                    //if user isn't an admin
                    const embed = new Discord.MessageEmbed();
                    embed.setColor(0x4a4a4a);
                    embed.setTitle("You don't have permission, fool " + msg.author.name.toUpperCase());
                    var rdm = parseInt(Math.random() * 100);
                    if (rdm <= 33) {
                        embed.setImage("https://thumbs.dreamstime.com/z/le-jeune-homme-dit-non-53544424.jpg");
                    }
                    else if (rdm <= 66) {
                        embed.setImage("https://thumbs.dreamstime.com/z/homme-disant-non-la-droite-d-arr%C3%AAt-l%C3%A0-37969703.jpg");
                    }
                    else {
                        embed.setImage("https://cdn.w600.comps.canstockphoto.com/non-photo-sous-licence_csp3451042.jpg");
                    }
                    msg.channel.send(embed);
                    return;
                }
            }
        }
    }
})

function shootCommand(msg){
    let author = msg.author;
    let mentionedUser = msg.mentions.users.get(msg.mentions.users.firstKey());

    const embed = new Discord.MessageEmbed();
    embed.setColor(0x4a4a4a);

    let rng = rollDiceWithMaxNumber(100);

    if (author === mentionedUser){ //if the user is shooting themselves
        if (rng <= 33) {
            embed.setTitle(author.name.toUpperCase() + " couldn't take the pressure and shot themselves");
            embed.setImage("https://t1.thpservices.com/previewimage/gallil/12b99a9df0c21c960c9ab9197acf8404/esy-007868283.jpg");
        }
        else if (rng <= 66) {
            embed.setTitle(author.name.toUpperCase() + " shot themselves to prove a point");
            embed.setImage("https://media.istockphoto.com/photos/man-gestures-to-shoot-self-picture-id155659399");
        }
        else {
            embed.setTitle(author.name.toUpperCase() + " shot themselves in the back");
            embed.setImage("https://i.imgflip.com/4wjgos.jpg");
        }
        msg.channel.send(embed);
        kill(msg, author);
        return;
    }


    if (rng <= 11) { //picks a random title and image (probably a better way to do these since there's a lot of else if but oh well)
        embed.setTitle("Bang, Bang, I shoot you with my gun " + mentionedUser.name);
        embed.setImage("https://www.gannett-cdn.com/presto/2019/06/17/PELM/983a66e3-664d-43ea-8123-69a178a21ef5-handgun.jpg?width=529&height=371&fit=crop&format=pjpg&auto=webp");
    }
    else if (rng <= 22) {
        embed.setTitle("WHY WONT YOU DIE ALREADY " + mentionedUser.name);
        embed.setImage("https://previews.123rf.com/images/aaronamat/aaronamat1201/aaronamat120100020/12104753-homme-tirant-un-pistolet-contre-un-mur-%C3%A9rod%C3%A9.jpg");
    }
    else if (rng <= 33) {
        embed.setTitle("Sorry kid, job's a job " + mentionedUser.name);
        embed.setImage("https://previews.123rf.com/images/sqback/sqback1710/sqback171000025/88576844-homme-%C3%A0-capuche-avec-un-pistolet-dans-l-obscurit%C3%A9.jpg");
    }
    else if (rng <= 44) {
        embed.setTitle("Reach for the skiesssss " + mentionedUser.name);
        embed.setImage("https://media.istockphoto.com/vectors/cowboy-shoot-a-gun-cartoon-vector-illustration-vector-id1142738877");
    }
    else if (rng <= 55) {
        embed.setTitle("UwU looks like this is it for you " + mentionedUser.name);
        embed.setImage("https://e7.pngegg.com/pngimages/143/943/png-clipart-doki-doki-literature-club-anime-manga-gun-firearm-anime-manga-cartoon.png");
    }
    else if (rng <= 66) {
        embed.setTitle("Repent " + mentionedUser.name);
        embed.setImage("https://st.depositphotos.com/1000975/2453/i/950/depositphotos_24539359-stock-photo-nun-with-gun-isolated-on.jpg");
    }
    else if (rng <= 77) {
        embed.setTitle("Darn you " + mentionedUser.name);
        embed.setImage("https://pbs.twimg.com/media/EBijQFMWkAA9vJV.jpg");
    }
    else if (rng <= 88) {
        embed.setTitle("Adding you to my epic frag compilation " + mentionedUser.name);
        embed.setImage("https://e7.pngegg.com/pngimages/581/234/png-clipart-fortnite-desktop-shooting-gun-fortnite-pc.png");
    }
    else if (rng <= 94) {
        embed.setTitle("Bang you're dead " + mentionedUser.name);
        embed.setImage("https://thumbs.dreamstime.com/z/man-shooting-gun-18646735.jpg");
    }
    else { //special case where there's a 6% chance to backfire
        embed.setTitle(author.name + " is dumb and accidentally shot themselves");
        embed.setImage("https://i.imgur.com/RThmrgN.png");
        msg.channel.send(embed);
        kill(msg, author);
        return;
    }
    msg.channel.send(embed);
    kill(msg, mentionedUser);
}

//this function serves to remove the shot user's role and add the dead role to them, which takes away all permissions from them
//it then gives it back and removes the dead role after 5 minutes
function kill(msg, person){
    let deadRole = msg.guild.roles.cache.get(deadRoleId);
    let userRole = msg.guild.member(person).roles.highest;

    person.roles.remove(userRole);
    person.roles.add(deadRole);

    setTimeout(function(){
        person.roles.remove(deadRole);
        person.roles.add(userRole);
    }, 300000);
}